import { ContextMessageUpdate } from "telegraf";
import { ConnectionPool }       from "mssql";
import { Chart }                from "../libs/RenderChart";
import { JsonToChartSeries }    from "../libs/TransformData";
import { RenderText }           from "../libs/RenderText";
import { numberWithCommas,parseDate,asString }     from "../libs/Utils";
var dateFormat = require('dateformat');
var nowzzz = new Date();
const extra = require('telegraf/extra')
const markup = extra.HTML()
export function PayloadHourly(ctx, pool: ConnectionPool,scheduler  ) {
    const title_atas = 'WFH Hourly Report Payload'
    const db = pool.connect()
    db.then(async(conn) => {
        const params =  scheduler.status?'':ctx.message.text.split(' ')
        var  datecmd =  params[1]?`'`+asString('yyyy-MM-dd', parseDate(params[1]))+`' AND Hour_Id = '${dateFormat(nowzzz, "H")}'+1`:"FORMAT ( GETDATE(), 'yyyy-MM-dd')"
        const chart = Chart(1368, 768)
        const { recordset } = await conn.query(`SELECT DATE	,MAX(Hour_Id) as h,
		Rank,
		Regional,
		AVG ( Remark ) AS Remark,
		SUM ( Payload_GB ) AS Payload_GB 
		FROM
		[RAFI2020].[dbo].[V_Covid_Increament_Payload_Region_Hourly] 
		WHERE
		DATE = ${datecmd} 
		GROUP BY
		DATE,
		Rank,
		Regional 
		ORDER BY
		rank 
        `)
        var hourz = `Hour : 00:00 to ${recordset[1].h}:00`
        const title = `${title_atas}\nDate : ${recordset[0]['DATE'].toISOString().substring(0, 10)}\n${recordset[1].h?hourz:''}`
        const text = RenderText(recordset, title, 
            ['Regional', 'Payload(GB)', 'Growth to Baseline(%)'], 
            ['Regional', 'Payload_GB', 'Remark'],
            {
                'Payload_GB': (val) => numberWithCommas(val.toFixed(0)),
                'Remark': (val) =>  val.toFixed(2)  + '%'
            },
            )
            const { datasets, series } = JsonToChartSeries(recordset, 'DATE', 'Regional', 'Remark')
            const chartConfig = {
                type: 'bar',
                data: {
                    series,
                    datasets,
                },
                options: {
                    title: {
                        display: true,
                        text: title_atas,
                        fontSize: '16'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return `${value} %`;
                                }
                            }
                        }]
                    }
                }
            }       
            if(scheduler.status){
                ctx.telegram.sendPhoto(scheduler.id,{ source: chart.renderToStream(chartConfig) })
                ctx.telegram.sendMessage(scheduler.id,text,markup)
            } else {
                ctx.replyWithPhoto({ source: chart.renderToStream(chartConfig) })
                ctx.replyWithHTML(text)
            }
        })
    }