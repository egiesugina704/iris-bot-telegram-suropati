import { ContextMessageUpdate } from "telegraf";
import { ConnectionPool }       from "mssql";
import { Chart }                from "../libs/RenderChart";
import { JsonToChartSeries }    from "../libs/TransformData";
import { RenderText }           from "../libs/RenderText";
import { numberWithCommas,parseDate,asString }     from "../libs/Utils";
const extra = require('telegraf/extra')
const markup = extra.HTML()
export function TrafficDaily(ctx, pool: ConnectionPool,scheduler  ) {
    const title_atas = 'WFH Daily Report Voice'
    const db = pool.connect()
    db.then(async(conn) => {
        const params =  scheduler.status?'':ctx.message.text.split(' ')
        var  datecmd =  params[1]?"'"+asString('yyyy-MM-dd', parseDate(params[1]))+"'":"FORMAT ( GETDATE()-1, 'yyyy-MM-dd')"
        const chart = Chart(1368, 768)
        const { recordset } = await conn.query(`Select *  from [RAFI2020].[dbo].[V_Covid_Increament_Traffic_Voice_Daily] where date = ${datecmd}
        order by rank
        `)
        const title = `${title_atas}\nDate : ${recordset[0]['date'].toISOString().substring(0, 10)} `
        const text = RenderText(recordset, title, 
            ['Regional', 'Traffic(erl)', 'Growth to Baseline(%)'], 
            ['Regional', 'Voice_Traffic', 'Remark'],
            {
                'Voice_Traffic': (val) => numberWithCommas(val.toFixed(0)),
                'Remark': (val) => val + '%'
            },
            )
            const { datasets, series } = JsonToChartSeries(recordset, 'date', 'Regional', 'Remark')
            const chartConfig = {
                type: 'bar',
                data: {
                    series,
                    datasets,
                },
                options: {
                    title: {
                        display: true,
                        text: title_atas,
                        fontSize: '16'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return `${value} %`;
                                }
                            }
                        }]
                    }
                }
            }       
            if(scheduler.status){
                ctx.telegram.sendPhoto(scheduler.id,{ source: chart.renderToStream(chartConfig) })
                ctx.telegram.sendMessage(scheduler.id,text,markup)
            } else {
                ctx.replyWithPhoto({ source: chart.renderToStream(chartConfig) })
                ctx.replyWithHTML(text)
            }
        })
    }