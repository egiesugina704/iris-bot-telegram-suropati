import { ContextMessageUpdate } from "telegraf";
import { ConnectionPool }       from "mssql";
import { Chart }                from "../libs/RenderChart";
import { JsonToChartSeries }    from "../libs/TransformData";
import { RenderText }           from "../libs/RenderText";
var dateFormat = require('dateformat');
var nowzzz = new Date();
import { numberWithCommas,parseDate,asString }     from "../libs/Utils";
const extra = require('telegraf/extra')
const markup = extra.HTML()
export function VLRHourly(ctx, pool: ConnectionPool,scheduler  ) {
    const title_atas = 'WFH Hourly Report VLR'
    const db = pool.connect()
    db.then(async(conn) => {
        const params =  scheduler.status?'':ctx.message.text.split(' ')
        var  datecmd =  params[1]?`'`+asString('yyyy-MM-dd', parseDate(params[1]))+`' AND Hour_Id = '${dateFormat(nowzzz, "H")}'+1`:"FORMAT ( GETDATE(), 'yyyy-MM-dd')"
        const chart = Chart(1368, 768)
        const { recordset } = await conn.query(`SELECT DATE	,MAX(Hour_Id) as h,
		Rank,
		Regional,
		round((MAX ([VLR_Subs]) - MAX ([VLR_Subs - 7])) / MAX ([VLR_Subs]) * 100 ,2) AS Remark,
		MAX ( [VLR_Subs] ) AS [VLR_Subs] 
		FROM
		[RAFI2020].[dbo].[V_Covid_Increament_VLR_Subs_Hourly]
		WHERE
		DATE = ${datecmd}  
		GROUP BY
		DATE,
		Rank,
		Regional 
		ORDER BY
		rank 
        `)
        var hourz = `Hour : 00:00 to ${recordset[1].h}:00`
        const title = `${title_atas}\nDate : ${recordset[0]['DATE'].toISOString().substring(0, 10)}\n${hourz}`
        const text = RenderText(recordset, title, 
            ['Regional', 'VLR Subs', 'Growth to Baseline(%)'], 
            ['Regional', 'VLR_Subs', 'Remark'],
            {
                'VLR_Subs': (val) => numberWithCommas(val),
                'Remark': (val) =>  val.toFixed(2)  + '%'
            },
            )
            const { datasets, series } = JsonToChartSeries(recordset, 'DATE', 'Regional', 'Remark')
            const chartConfig = {
                type: 'bar',
                data: {
                    series,
                    datasets,
                },
                options: {
                    title: {
                        display: true,
                        text: title_atas,
                        fontSize: '16'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return `${value} %`;
                                }
                            }
                        }]
                    }
                }
            }
            console.log(scheduler)       
            if(scheduler.status){
                ctx.telegram.sendPhoto(scheduler.id,{ source: chart.renderToStream(chartConfig) })
                ctx.telegram.sendMessage(scheduler.id,text,markup)
            } else {
                ctx.replyWithPhoto({ source: chart.renderToStream(chartConfig) })
                ctx.replyWithHTML(text)
            }
        })
    }