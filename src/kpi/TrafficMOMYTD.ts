import { ContextMessageUpdate } from "telegraf";
import { ConnectionPool }       from "mssql";
import { Chart }                from "../libs/RenderChart";
import { JsonToChartSeries }    from "../libs/TransformData";
import { RenderText }           from "../libs/RenderText";
import { numberWithCommas,parseDate,asString }     from "../libs/Utils";
const extra = require('telegraf/extra')
const markup = extra.HTML()
export function TrafficMOMYTD(ctx, pool: ConnectionPool,scheduler) {
    const title_atas = 'WFH Daily Report Traffic'
    const db = pool.connect()
    db.then(async(conn) => {
        const params =  scheduler.status?'':ctx.message.text.split(' ')
        var  datecmd =  params[1]?"'"+asString('yyyy-MM-dd', parseDate(params[1]))+"'":""
        const chart = Chart(1368, 768)
        const { recordset } = await conn.query(`EXEC [RAFI2020].[bot].[region_traffic_wowmomyoy] ${datecmd}`)
        const title = `${title_atas}\nDate : ${recordset[0]['date'].toISOString().substring(0, 10)} `
        const text = RenderText(recordset, title, 
            ['Regional', 'MOM','YTD'], 
            ['Regional', 'MOM','YTD'],
            {
                //'Traffic(Erl)': (val) => numberWithCommas(val.toFixed(0)),
                'MOM': (val) => val.toFixed(2) + '%',
                'YTD': (val) =>  val.toFixed(2) + '%'
            },
            )
            const { datasets, series } = JsonToChartSeries(recordset, 'date', 'Regional', 'MOM')
            const chartConfig = {
                type: 'bar',
                data: {
                    series,
                    datasets,
                },
                options: {
                    title: {
                        display: true,
                        text: 'WFH Daily Report Traffic MOM',
                        fontSize: '16'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return `${value} %`;
                                }
                            }
                        }]
                    }
                }
            }
            if(scheduler.status){
                ctx.telegram.sendPhoto(scheduler.id,{ source: chart.renderToStream(chartConfig) })
                ctx.telegram.sendMessage(scheduler.id,text,markup)
            } else {
               // ctx.replyWithPhoto({ source: chart.renderToStream(chartConfig) })
                ctx.replyWithHTML(text)
            }     
        })
    }