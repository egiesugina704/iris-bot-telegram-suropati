import { ContextMessageUpdate } from "telegraf";
import { ConnectionPool }       from "mssql";
import { Chart }                from "../libs/RenderChart";
import { JsonToChartSeries }    from "../libs/TransformData";
import { RenderText }           from "../libs/RenderText";
import { numberWithCommas,parseDate,asString }     from "../libs/Utils";
const extra = require('telegraf/extra')
const markup = extra.HTML()
export function Internet(ctx, pool: ConnectionPool,scheduler  ) {
    const title_atas = 'WFH Hourly Report'
    const db = pool.connect()
    db.then(async(conn) => {
        const params =  scheduler.status?'':ctx.message.text.split(' ')
        var  datecmd =  params[1]?"'"+asString('yyyy-MM-dd', parseDate(params[1]))+"'":"FORMAT ( GETDATE()-1, 'yyyy-MM-dd')"
        const chart = Chart(1368, 768)
        const { recordset } = await conn.query(`SELECT  * FROM [RAFI2020].[dbo].[V_Covid_Incraement_Internet_Thp_Daily] where Date = ${datecmd}`)
         
            in2(ctx , pool ,scheduler,recordset )
        //
           
            //ctx.replyWithHTML(text)
        })
    }

    function in2(ctx, pool: ConnectionPool,scheduler  ,data1){
        const db = pool.connect()
        db.then(async(conn) => {
            const params =  scheduler.status?'':ctx.message.text.split(' ')
            var  datecmd =  params[1]?"'"+asString('yyyy-MM-dd', parseDate(params[1]))+"'":"FORMAT ( GETDATE()-1, 'yyyy-MM-dd')"
            const chart = Chart(1368, 768)
            const { recordset } = await conn.query(`SELECT  * FROM [RAFI2020].[dbo].[V_Covid_Incraement_THP_Area_Daily] where date_id = ${datecmd} order by Area`)
            sends(ctx ,recordset,data1,scheduler)
               
            })}

            function sends(ctx,result2,result,scheduler){
                const title = `<b>WFH Daily Report\nDate : ${result[0]['Date'].toISOString().substring(0, 10)} </b>`
                console.log("last")
                var msg = `${title}\n\n<b>Throughput GiLan Update\nService | Throughput (Mbps) | Capacity (Mbps) | Occ (%) |  Growth to Baseline (%)</b>`
                var x : any
                for(x in result){
                    msg += `\n${result[x].ne_name} | ${numberWithCommas(Number((result[x].throughput).toFixed(0)))} | ${numberWithCommas(Number((result[x].capacity).toFixed(0)))} | ${numberWithCommas(Number((result[x].occupancy).toFixed(0)))}% | ${Number((result[x].remarkThroughput).toFixed(2))}%`
                }
                msg += `\n\n<b>Throughput GGSN Increment Update\nArea | Throughput (Mbps) | Capacity (Mbps) | Occ (%) |  Growth to Baseline (%)</b>`
                var z : any
                for(z in result2){
                    msg += `\n${result2[z].Area} | ${numberWithCommas(Number((result2[z].GGSN_THP_Usage).toFixed(0)))} | ${numberWithCommas(Number((result2[z].GGSN_THP_Capacity).toFixed(0)))} | ${numberWithCommas(Number((result2[z].GGSN_THP_Occupancy).toFixed(0)))}% | ${Number((result2[z].remarkThroughput).toFixed(2))}%`
                }
                //return  ctx.replyWithHTML(msg)   
                if(scheduler.status){ 
                    ctx.telegram.sendMessage(scheduler.id,msg,markup)
                } else {
                    ctx.replyWithHTML(msg)
                }    
            }