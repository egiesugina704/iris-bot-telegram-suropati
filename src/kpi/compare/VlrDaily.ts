import { ContextMessageUpdate } from "telegraf";
import { ConnectionPool }       from "mssql";
import { Chart }                from "../../libs/RenderChart";
import { JsonToChartSeries }    from "../../libs/TransformData";
import { RenderText }           from "../../libs/RenderText";
import { numberWithCommas,parseDate,asString }     from "../../libs/Utils";
export function VLRDailyCOMP(ctx: ContextMessageUpdate, pool: ConnectionPool,scheduler  ) {
    const params = ctx.message.text.split(' ')
    var datess = params[2]?params[2]:params[1]
    var datecmd =  datess?"'"+asString('yyyy-MM-dd', parseDate(datess))+"'":"FORMAT ( GETDATE(), 'yyyy-MM-dd')"
    //var reg = params[2]?params[1]=='All'||params[1]=='all'?"rank not in(13,15,14)":"rank in(13,15,14)":"rank not in(13,15,14)"
    var reg = params[2]?params[1]=='N'||params[1]=='n'?"Rank_2 in(13,15,14)":params[1]=='R'||params[1]=='r'?"Rank_2 not in(13,15,14)":`Rank_2 = ${params[1].slice(1,4)}`:"Rank_2 not in(13,15,14)"
    var daysmin = params[3]?params[3]:30
    const db = pool.connect()
    db.then(async(conn) => {
        const { recordset } = await conn.query(`select * from 
        [RAFI2020].[dbo].[V_Covid_Increament_VLR_Subs_Daily] 
        where 
        date  
        BETWEEN   
        DATEADD(day, -${daysmin}, Convert(varchar(30),${datecmd},120))   AND  ${datecmd} 
        and ${reg}`)
        const chart = Chart(1368, 768)
        const { datasets, labels } = JsonToChartSeries(recordset, 'date', 'Regional', 'VLR_Subs')
        const chartConfig = {
            type: 'line',
            data: {
                labels,
                datasets
            },
            options: {
                title: {
                    display: true,
                    text: 'WFH Daily Report VLR',
                    fontSize: '16'
                },
                legend: {
                    position: 'right',
                    align: 'start',
                    labels: {
                        boxWidth: 20
                    }
                }
            }
        }       
        
        ctx.replyWithPhoto({ source: chart.renderToStream(chartConfig) })
    })
    }