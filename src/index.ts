
require('dotenv').config()
import Log = require('log4js')
const logger = Log.getLogger()
logger.level = 'debug';
const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('./db.json')
const middlewares = jsonServer.defaults()

server.use(middlewares)
server.use(router)
server.listen(process.env.PORT_DBJSON, () => {
    logger.info('JSON Server is running')
})
//=========================================================
import Telegraf from 'telegraf'
const HttpsProxyAgent = require('https-proxy-agent');
const url = require('request');
import { ConnectionPool } from 'mssql'
const schedule = require('node-schedule');
const extra = require('telegraf/extra')
const markup = extra.HTML()
var scheduler = { status: false, id: 0 }
import { SchReq, Register, Approve, NotRegister, Auth } from './libs/auth'
import {
    PayloadDaily,
    SMSDaily,
    TrafficDaily,
    SMSHourly,
    PayloadHourly,
    TrafficHourly,
    VlrDaily,
    Internet,
    SMSDailyCOMP,
    SMSHourlyCOMP,
    PayloadDailyCOMP,
    PayloadHourlyCOMP,
    TrafficDailyCOMP,
    TrafficHourlyCOMP,
    VLRDailyCOMP,
    PayloadMOMYTD,
    SMSMOMYTD,
    TrafficMOMYTD,
    VLRMOMYTD
} from './kpi'
import { VLRHourly } from './kpi/VlrHourly';
import { VLRHourlyCOMP } from './kpi/compare/VlrHourly';




//database config
const databaseConfig = {
    user: process.env.USER,
    password: process.env.PASSWORD,
    server: process.env.SERVER,
    database: process.env.DATABASE,
    requestTimeout: 900000,
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    }
}
const pool = new ConnectionPool(databaseConfig)

const bot = new Telegraf(process.env.TOKEN, { //TOKEN BOT
    // telegram: {
    // 	agent: new HttpsProxyAgent('http://10.59.82.5:8080/')
    // }
})

bot.on('message', (ctx, next) => {
    logger.info(ctx.message.text)
    return next()
})

bot.help(ctx => {
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            ctx.replyWithHTML(`
    <b><u>Suropati BOT</u></b>
    
    <ins>List Command KPI Daily:</ins>

    /payload
    /trendpayload
    /payloadmomytd

    /sms
    /trendsms
    /smsmomytd

    /voice
    /trendvoice
    /trafficmomytd

    /vlr
    /trendvlr
    /vlrmomytd
    /internet
    

    <ins>List Command KPI Hourly:</ins>
    /payloadhourly
    /trendpayloadhourly

    /smshourly
    /trendsmshourly

    /voicehourly
    /trendvoicehourly

    /vlrhourly
    /trendvlrhourly
    
    
    <ins>Get Data with specific date:</ins>
    Format:
    <code>/kpi yyyymmdd</code>
    <code>/trendkpi Rn yyyymmdd days</code>
    
    Example:
    <code>/payload 20200315</code>
    <code>/trendpayload R1 20200315 7</code>
    
    Auto Generate KPI:
    /scheduler
    `)
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/scheduler', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SchReq(ctx)
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/register', (ctx) => {
    scheduler = { status: false, id: 0 }
    return Register(ctx)
})
bot.command('/confirm', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return Approve(ctx)
        } else {
            return ctx.reply(`${ctx.message.chat.title ? ctx.message.chat.title : ctx.message.from.first_name}, You do not have acces`)
        }
    })
})
bot.command('/testauth', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/payload', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/sms', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/voice', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/vlr', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VlrDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/internet', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return Internet(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/smshourly', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSHourly(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/payloadhourly', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadHourly(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/voicehourly', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficHourly(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/vlrhourly', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VLRHourly(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendsms', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSDailyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendsmshourly', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSHourlyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendpayload', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadDailyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendpayloadhourly', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadHourlyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendvoice', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficDailyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendvoicehourly', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficHourlyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendvlr', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VLRDailyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendvlrhourly', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VLRHourlyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})

bot.command('/payloadmomytd', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadMOMYTD(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/smsmomytd', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSMOMYTD(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trafficmomytd', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficMOMYTD(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/vlrmomytd', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VLRMOMYTD(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
//===============================
bot.command('/payload@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/sms@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/voice@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/vlr@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VlrDaily(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/internet@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return Internet(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/smshourly@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSHourly(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/payloadhourly@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadHourly(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/voicehourly@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficHourly(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/vlrhourly@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VLRHourly(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendsms@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSDailyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendsmshourly@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSHourlyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendpayload@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadDailyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendpayloadhourly@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadHourlyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendvoice@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficDailyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendvoicehourly@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficHourlyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendvlr@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VLRDailyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trendvlrhourly@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VLRHourlyCOMP(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})

bot.command('/payloadmomytd@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return PayloadMOMYTD(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/smsmomytd@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return SMSMOMYTD(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/trafficmomytd@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return TrafficMOMYTD(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})
bot.command('/vlrmomytd@isurobot', (ctx) => {
    scheduler = { status: false, id: 0 }
    var idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        if (JSON.parse(body).id == idx) {
            return VLRMOMYTD(ctx, pool, scheduler);
        } else {
            return NotRegister(ctx)
        }
    })
})


bot.on('text', (ctx) => {
    var cmd = ctx.update.message.text
    if (cmd.slice(0, 1) == '/') {
        return ctx.reply(`Command ${cmd} undefined,  Show command Suropati type /help`)
    }
})
const timeout = ms => new Promise(res => setTimeout(res, ms))
async function daily_auto(xx) {
    PayloadDaily(bot, pool, xx)
    await timeout(5000)
    SMSDaily(bot, pool, xx)
    await timeout(5000)
    TrafficDaily(bot, pool, xx)
    await timeout(5000)
    VlrDaily(bot, pool, xx)
    await timeout(5000)
    Internet(bot, pool, xx)
    scheduler = { status: false, id: 0 }
}
async function hourly_auto(xx) {
    SMSHourly(bot, pool, xx)
    await timeout(5000)
    PayloadHourly(bot, pool, xx)
    await timeout(5000)
    TrafficHourly(bot, pool, xx)
    await timeout(5000)
    VLRHourly(bot, pool, xx)
    scheduler = { status: false, id: 0 }
}
var a = schedule.scheduleJob({ hour: 12, minute: 1 }, function () {
    url('http://127.0.0.1:${process.env.PORT_DBJSON}/sch', function (error, response, body) {
        Array.prototype.forEach.call(JSON.parse(response.body), child => {
            var xx = { status: true, id: child.id }
            console.log(xx)
            daily_auto(xx)
            scheduler = { status: false, id: 0 }
        })
    })
});
var b = schedule.scheduleJob({ hour: 7, minute: 1 }, function () {
    url('http://127.0.0.1:${process.env.PORT_DBJSON}/sch', function (error, response, body) {
        Array.prototype.forEach.call(JSON.parse(response.body), child => {
            var xx = { status: true, id: child.id }
            console.log(xx)
            daily_auto(xx)
            scheduler = { status: false, id: 0 }
        })
    })
});
var c = schedule.scheduleJob({ hour: 17, minute: 1 }, function () {
    url('http://127.0.0.1:${process.env.PORT_DBJSON}/sch', function (error, response, body) {
        Array.prototype.forEach.call(JSON.parse(response.body), child => {
            var xx = { status: true, id: child.id }
            console.log(xx)
            hourly_auto(xx)
            scheduler = { status: false, id: 0 }
        })
    })
});
var c = schedule.scheduleJob({ hour: 21, minute: 1 }, function () {
    url('http://127.0.0.1:${process.env.PORT_DBJSON}/sch', function (error, response, body) {
        Array.prototype.forEach.call(JSON.parse(response.body), child => {
            var xx = { status: true, id: child.id }
            console.log(xx)
            hourly_auto(xx)
            scheduler = { status: false, id: 0 }
        })
    })
});
logger.info('Tele Bot Started')
//bot.launch()
bot.startPolling()
