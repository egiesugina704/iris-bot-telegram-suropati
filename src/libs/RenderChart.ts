import { CanvasRenderService } from 'chartjs-node-canvas'

const chartCallback = (ChartJS) => {
    ChartJS.defaults.global.elements.rectangle.borderWidth = 2;
    ChartJS.plugins.register({
    });
    ChartJS.controllers.MyType = ChartJS.DatasetController.extend({
    });
};

export function Chart (width, height) {
    return new CanvasRenderService(width, height, chartCallback);
}