export function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
export function parseDate(input) {
	var y = input.slice(0,4)
	var m  = input.slice(4,6)
	var d = input.slice(6,8)
	return new Date(y,m-1, d)
}

export function padWithZeros(vNumber, width) {
    var numAsString = vNumber.toString();
    while (numAsString.length < width) {
      numAsString = "0" + numAsString;
    }
    return numAsString;
  }
  
  export function addZero(vNumber) {
    return padWithZeros(vNumber, 2);
  }
  export function offset(timezoneOffset) {
    var os = Math.abs(timezoneOffset);
    var h = String(Math.floor(os / 60));
    var m = String(os % 60);
    if (h.length === 1) {
      h = "0" + h;
    }
    if (m.length === 1) {
      m = "0" + m;
    }
    return timezoneOffset < 0 ? "+" + h + m : "-" + h + m;
  }
  
export function asString(format, date) {
    if (typeof format !== "string") {
      date = format;
      format = module.exports.ISO8601_FORMAT;
    }
    if (!date) {
      date = module.exports.now();
    }
  
    // Issue # 14 - Per ISO8601 standard, the time string should be local time
    // with timezone info.
    // See https://en.wikipedia.org/wiki/ISO_8601 section "Time offsets from UTC"
  
    var vDay = addZero(date.getDate());
    var vMonth = addZero(date.getMonth() + 1);
    var vYearLong = addZero(date.getFullYear());
    var vYearShort = addZero(vYearLong.substring(2, 4));
    var vYear = format.indexOf("yyyy") > -1 ? vYearLong : vYearShort;
    var vHour = addZero(date.getHours());
    var vMinute = addZero(date.getMinutes());
    var vSecond = addZero(date.getSeconds());
    var vMillisecond = padWithZeros(date.getMilliseconds(), 3);
    var vTimeZone = offset(date.getTimezoneOffset());
    var formatted = format
      .replace(/dd/g, vDay)
      .replace(/MM/g, vMonth)
      .replace(/y{1,4}/g, vYear)
      .replace(/hh/g, vHour)
      .replace(/mm/g, vMinute)
      .replace(/ss/g, vSecond)
      .replace(/SSS/g, vMillisecond)
      .replace(/O/g, vTimeZone);
    return formatted;
  }