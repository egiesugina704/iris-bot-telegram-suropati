const url = require('request');
import { ContextMessageUpdate } from "telegraf";
const extra = require('telegraf/extra')
const markup = extra.HTML()
export function SchReq (ctx){
    //console.log(`User ${JSON.stringify(ctx.message.chat)}`)
    var datas =  ctx.message.chat
    url.post({url:'http://127.0.0.1:${process.env.PORT_DBJSON}/sch', 
    form: datas}, 
    function onSucsess(err,httpResponse,body){ 
        return ctx.replyWithHTML(`
        <b><u>Scheduler Active </u></b>
        
        <code> 07:00 AM (Daily D-1)</code>
        <code> 12:00 AM (Daily D-1)</code>
        <code> 05:00 PM (Hourly 0-x)</code>
        <code> 09:00 PM (Hourly 0-x)</code>`)
    },function onError(err){
        return ctx.reply(err)
    })
}
export function Register (ctx){
    //console.log(`User ${JSON.stringify(ctx.message.chat)}`)
    var datas =  ctx.message.chat
    url.post({url:'http://127.0.0.1:${process.env.PORT_DBJSON}/waitingliist', 
    form: datas}, 
    function onSucsess(err,httpResponse,body){
        return ctx.reply('Waiting for Confirmation')
    },function onError(err){
        return ctx.reply(err)
    })
    url('http://127.0.0.1:${process.env.PORT_DBJSON}/admin', function (error, response, body) {
    Array.prototype.forEach.call(JSON.parse(response.body),  child  => {
        var a =`
        <b>New Register</b>
        
        ID            : ${datas.id}
        Username : ${datas.username}
        Nama       : ${datas.first_name?datas.first_name:datas.title} ${datas.last_name?datas.last_name:''}
        Type        : ${datas.type}
        
        <code>/confirm ${datas.id}</code>`
        ctx.telegram.sendMessage(child.id,a,markup)
    })})
}
export function Approve (ctx){
    //console.log(`User ${JSON.stringify(ctx.message.chat)}`)
    var iduser = ctx.update.message.text.split(' ')
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/waitingliist/${iduser[1]}`, function (error, response, body) {
       var data = JSON.parse(body)
       console.log(data)
    url.post({url:`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/`, 
    form: data}, 
    function onSucsess(err,httpResponse,body){
        var a = `
    <b><u>Suropati BOT</u></b>
    
    <ins>List Command KPI Daily:</ins>
    /payload
    /trendpayload
    /sms
    /trendsms
    /voice
    /trendvoice
    /vlr
    /trendvlr
    /internet
    
    <ins>List Command KPI Hourly:</ins>
    /payloadhourly
    /trendpayloadhourly
    /smshourly
    /trendsmshourly
    /voicehourly
    /trendvoicehourly
    
    
    <ins>Get Data with specific date:</ins>
    Format:
    <code>/kpi yyyymmdd</code>
    <code>/trendkpi Rn yyyymmdd days</code>
    
    Example:
    <code>/payload 20200315</code>
    <code>/trendpayload R1 20200315 7</code>
    
    Auto Generate KPI:
    /scheduler
    `
        ctx.telegram.sendMessage(iduser[1],a,markup)
        ctx.reply('Done')
    },function onError(err){
        return ctx.reply(err)
    })
}) 
//url.delete(`http://127.0.0.1:${process.env.PORT_DBJSON}/waitingliist/${iduser[1]}`)
}
export function NotRegister(ctx){
    console.log("Data")
    return ctx.reply(`${ctx.message.chat.title?ctx.message.chat.title:ctx.message.from.first_name}, You do not have access, please register. /register `)
}

export function Auth(ctx){
    var  idx = ctx.message.chat.id
    url(`http://127.0.0.1:${process.env.PORT_DBJSON}/whitelist/${idx}`, function (error, response, body) {
        console.log(body.id)
        if (body.id) {
            return true;
        } else {
            return NotRegister(ctx)
        }
    })
}